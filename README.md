# tracker-list

List of trackers, used by PrivacyCentral app to detect and filter trackers. The list is a merge of Exodus, WhoTracksMe and StevenBlack trackers lists, with custom /e/ rules.
The list is located in list/e_trackers.json

The list is automatically upgraded each day (if modified) with a CI script.

## Manualy upgrade list

List can be manualy updated :

1. build a new list : 
```
$ node build_list.js
Fetch and parse Whotracksme OK - 2404 trackers
Fetch and parse Exodus OK - 260 trackers
updateWithTrackerList - baseList 3784 with otherList 260
updateWithTrackerList - remaining Others: 0
updateWithTrackerList now total of baseList 3784
updateWithTrackerList - baseList 3784 with otherList 2404
updateWithTrackerList - remaining Others: 0
updateWithTrackerList now total of baseList 3784
1 hostnames lists with each : 141886 hostnames
hostnames total : 141886
0 trackers deprecated, to remove
Trackers list finished, 3784 trackers, 21224 hostnames
No changes in trackers, abort updating.

```
2. Chech validity of the list :
```
$ node validate_json.js 

JSON format is as expected
```
3. Commit the just generated version of list/e_trackers.json
