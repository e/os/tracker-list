const validate = require('jsonschema').validate

const data = require('./list/e_trackers.json')
const schema = require('./trackers_list_schema.json')


const report = validate(data, schema)
const valid = report.valid && data.trackers.length > 400

if (valid) {
    console.log("JSON format is as expected")
} else {
    console.log("Invalid trackers list: ")
    console.log(report)

    process.exit(1)
}