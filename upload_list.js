const https = require('https')
const fs = require('fs')
const url = require('url')

const outputFile = "list/e_trackers.json"

const ciJobToken = process.env.CI_JOB_TOKEN
const ciApiV4Url = process.env.CI_API_V4_URL
const giltabProjectId = "1183"

const trackersList = require('./' + outputFile)

function uploadTrackers(trackersList) { return new Promise((resolve, reject) => {
    const body = JSON.stringify({
        content: JSON.stringify(trackersList),
        branch: 'main',
        commit_message: `Update trackers list ; ${trackersList.trackers.length} trackers`
    })

    const urlComponents = url.parse(`${ciApiV4Url}/projects/${encodeURIComponent(giltabProjectId)}/repository/files/${encodeURIComponent(outputFile)}`)

    const options = {
        method: 'PUT',
        port: 443,
        hostname: urlComponents.hostname,
        path: urlComponents.path,
        headers: {
            'PRIVATE-TOKEN': ciJobToken,
            'Content-Type': 'application/json',
            'Content-Length': body.length,
        }
    }

    const req = https.request(options, response => {
        let data = ""
        response.on('data', (chunk) => { data += chunk })
        response.on('end', () => resolve(data))
    })
    req.on('error', reject)

    req.write(body)
    req.end()
})}

uploadTrackers(trackersList).then(data => {
    console.log(`upload response: ${data}`)
    console.log("DONE")
}).catch(err => {
    console.log(err)
    process.exit(1)
})
