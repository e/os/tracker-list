const exodusUri = "https://reports.exodus-privacy.eu.org/api/trackers"

const fetcher = require("./fetch_promise.js")

function exodusToETrackers(exodusTrackers) {
    return Object.values(exodusTrackers.trackers)
    .filter(tracker => tracker.network_signature != "")
    .map(tracker => {
        const id = "exodus_" + tracker.id
        const hostnames = tracker.network_signature
            .replace(/\\/g, "")
            .split("|")
        return {
            id: id,
            hostnames: hostnames,
            name: tracker.name,
            exodusId: tracker.id,
            link: "https://reports.exodus-privacy.eu.org/trackers/" + tracker.id
        }
    })
}

exports.getTrackers = () => fetcher.fetch(exodusUri)
  .then(response => { 
    const exodusData = JSON.parse(response) 
    const exodusTrackers = exodusToETrackers(exodusData)
    console.log("Fetch and parse Exodus OK - " + exodusTrackers.length + " trackers")
    return exodusTrackers
  })

