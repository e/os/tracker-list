const https = require('https')

function fetch(url) {
    return new Promise((resolve, reject) => {
      https.get(url, (response) => {
            let data = ""
            response.on('data', (chunk) => { data += chunk })
            response.on('end', () => resolve(data))
        }).on('error', reject)      
    })
}

exports.fetch = fetch

