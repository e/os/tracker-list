const adawayUri = "https://adaway.org/hosts.txt"
const stevenblackUri = "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"

const fetcher = require("./fetch_promise.js")

function parseHostToHostnames(hostText) {
  return hostText
    .split("\n")
    .filter(line => line.startsWith("0.0.0.0"))
    .map(line => line.split(" ")[1])
    .filter(it => it != "")
}

function fetchHostnamesList(uri) {
  return fetcher.fetch(uri).then(hosts => parseHostToHostnames(hosts))
}

exports.getHostnames = () => Promise.all([
  // Adaway is included in the setvenblack list, so we don't fetch it anymore.
  // https://github.com/StevenBlack/hosts/blob/master/readme.md#sources-of-hosts-data-unified-in-this-variant
  fetchHostnamesList(stevenblackUri)
]).then(hostnameLists => {
  console.log(hostnameLists.length + " hostnames lists with each : " + hostnameLists.map(it => it.length + " hostnames").join(", "))
  const hostnames = new Set(hostnameLists.reduce((acc, hostnameList) => acc.concat(hostnameList)))
  console.log("hostnames total : " + hostnames.size)
  return hostnames
})

