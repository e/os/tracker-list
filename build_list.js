const EXIT_CODE_NO_CHANGES = 5

const whitelistedDomains = [
    "login.microsoftonline.com",
]

const outputFile = "./list/e_trackers.json"

const fs = require('fs')

const exodus = require('./src/exodus_trackers.js')
const whotracksme = require('./src/whotracksme_trackers.js')
const hostsfiles = require('./src/hostsfiles_hostnames.js')


const eTrackers = require("./" + outputFile).trackers.map(it => {
    it.dirty = true
    return it 
}).sort((a, b) => b.id.localeCompare(a.id))

function remove(list, item) {
  list.splice(list.indexOf(item), 1)
}

function updateWithTrackerList(baseList, otherList) {
  const mergeTrackers = (base, other) => {
    base.hostnames = Array.from(new Set(base.hostnames.concat(other.hostnames)))
    base.name = other.name
    base.dirty = undefined
    base.link = (base.link) ? base.link : other.link
    base.exodusId = (base.exodusId) ? base.exodusId : other.exodusId
  }

  console.log("updateWithTrackerList - baseList " + baseList.length + " with otherList " + otherList.length)
  baseList.forEach(tracker => {    
    const sameId = otherList.find(candidate => candidate.id == tracker.id)
      if (sameId) {
        mergeTrackers(tracker, sameId)
        remove(otherList, sameId)
      } else {
        otherList.some(candidate => {
          if (tracker.hostnames.some(host => candidate.hostnames.some(h2 => h2 == host))) {
	    mergeTrackers(tracker, candidate)
            remove(otherList, candidate)
            return true
          } else {
            return false
          }
        })
      }
   })
    
   console.log("updateWithTrackerList - remaining Others: " + otherList.length)
  
   Array.prototype.push.apply(baseList, otherList)
   console.log("updateWithTrackerList now total of baseList " + baseList.length)
}

function extractSubhostnamesToTracker(hostname, tracker, hostnameSet) {
  const toDelete = new Set()
  hostnameSet.forEach(subhost => {
    if (subhost.endsWith(hostname)) {
      if (!tracker.hostnames.includes(subhost)) {
        tracker.hostnames.push(subhost)
      }
      tracker.dirty = undefined
      toDelete.add(subhost)
    }
  })
  toDelete.forEach(it => hostnameSet.delete(it))
}


function applyOutbrainRule(trackersList, hostnameSet) {
  const tracker = trackersList.find(it => it.name === "OutBrain")
  if (tracker != null) {
    extractSubhostnamesToTracker('outbrainimg.com', tracker, hostnameSet)
  }
}

function enrichTrackersWithHostnames(trackerList, hostnameSet) {
  trackerList.forEach(tracker => {
    tracker.hostnames.forEach(hostname => {
      extractSubhostnamesToTracker(hostname, tracker, hostnameSet)
    })
  })
}

function keepOnlyHostnamesInHostnamesSet(trackerList, hostnameSet) {
  console.log("hostnameSet.size : " + hostnameSet.size)
  const deleted = new Set()
  trackerList.forEach(tracker => {
    tracker.hostnames
      .filter(it => it.split(".").length == 2 && !hostnameSet.has(it))
      .forEach(it => {
        remove(tracker.hostnames, it)
        deleted.add(it)
      })
  })
  console.log("Deleted hostnames not in hostnames list : " + deleted.size)
}

function removeWhitelistedHostnames(trackersList) {
   var emptyTrackersIndexes = []
   trackersList.forEach((tracker, index) => { 
   	tracker.hostnames = tracker.hostnames.filter(host => whitelistedDomains.indexOf(host) < 0)
   	if (tracker.hostnames.length == 0) {
            emptyTrackersIndexes.unshift(index)
        }    
   })
   emptyTrackersIndexes.forEach(index => trackersList.splice(index, 1))
}

function listHasNoChanges() {
    // Compare actual list with the builded one (without the createdAt field!)
    const oldTrackers = require("./" + outputFile).trackers
    return JSON.stringify(eTrackers) == JSON.stringify(oldTrackers)
}


function printTrackers() {    
    return JSON.stringify({
        trackers: eTrackers,
        createdAt: new Date().toISOString()
    })
}

function saveTrackersFile(trackersJson) {
    fs.writeFileSync(outputFile, trackersJson)
}


Promise.all([
    exodus.getTrackers(),
    whotracksme.getTrackers()
]).then(trackerLists => {
    trackerLists.forEach(otherList => updateWithTrackerList(eTrackers, otherList))
    return eTrackers
}).then(eTrackers => hostsfiles.getHostnames()
).then(hostnameSet => {
    const hostnameSetToConsume = new Set(hostnameSet)
    applyOutbrainRule(eTrackers, hostnameSetToConsume)
    enrichTrackersWithHostnames(eTrackers, hostnameSetToConsume)
    
    // Remove 10% of the hostnames, 
    // but should avoid unaccessible sites like adob.com or snapchat.com
    keepOnlyHostnamesInHostnamesSet(eTrackers, hostnameSet)
    removeWhitelistedHostnames(eTrackers)
    return eTrackers
}).then(eTrackers => {
  const deprecatedTrackers = eTrackers.filter(it => it.dirty)
  console.log(deprecatedTrackers.length + " trackers deprecated, to remove")
  if (deprecatedTrackers.length > 0) {
    console.log(deprecatedTrackers.map(it => it.name))
    console.log("before clean up: " + eTrackers.length)
    eTrackers.filter(it => it.dirty).forEach(it => {
      remove(eTrackers, it)
    })
    console.log("after clean up: " + eTrackers.length)
  }
   
  const trackersCount = eTrackers.length
  const hostnamesCount = eTrackers.reduce((acc, tracker) => acc + tracker.hostnames.length, 0)
  console.log("Trackers list finished, " + trackersCount + " trackers, " + hostnamesCount + " hostnames")
  return eTrackers
})
.then(trackers => {
  if (listHasNoChanges()) {
    process.exitCode = EXIT_CODE_NO_CHANGES
    throw "No changes in trackers, abort updating."
  }
})
.then(toto => {
  return printTrackers()
})
.then(trackersJson => {
  return saveTrackersFile(trackersJson)
})
.then(toto => {
  console.log("DONE")
})
.catch(err => {
  if (process.exitCode === undefined) {
    process.exitCode = 1
  }
  console.log(err)
})

