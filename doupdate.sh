#!/bin/sh


node build_list.js

RETVAL=$?

case "$RETVAL" in
    "0")
        node validate_json.js
        git add list/e_trackers.json
        git commit -m "Auto update trackers list"
        git push origin HEAD:main
        ;;
    "5")
        RETVAL=0
        ;;
    *)
        ;;
esac

exit $RETVAL
